package id.ac.ub.papb.recycler1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class Activity2 extends AppCompatActivity {

    TextView tvNim;
    TextView tvNama;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);
        tvNim = findViewById(R.id.tvNim2);
        tvNama =  findViewById(R.id.tvNama2);
        tvNim.setText("NIM : " + getIntent().getStringExtra("nim"));
        tvNama.setText("Nama : " + getIntent().getStringExtra("nama"));
    }
}